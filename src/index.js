import ReactDOM from "react-dom";
import React from "react";
import App from "./app";
import './index.css';
import { SettingsProvider } from "./common/settings-ctx";
import "./i18n";

ReactDOM.render(
    <SettingsProvider>
        <App />
    </SettingsProvider>
    , document.getElementById("root"));
