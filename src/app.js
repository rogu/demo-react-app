import { BrowserRouter, Route, Routes, Outlet } from "react-router-dom";
import { Workers } from "./pages/workers/workers";
import { Items } from "./pages/items/items";
import Item from "./pages/items/item";
import { Worker } from "./pages/workers/worker";
import { SettingsConsumer } from "./common/settings-ctx";
import { WorkersContextProvider } from "./pages/workers/workers-ctx";
import { ItemsContextProvider } from "./pages/items/items-ctx";
import { Register } from "./pages/register/register";
import { Profile } from "./pages/profile/profile";
import { Home } from "./pages/home/home";
import MainAppBar from "./components/menu";
import Spinner from "./components/spinner";

const App = () => {
  return (
    <>
      <SettingsConsumer>
        {({ theme, version, loading }) => (
          <div className={theme}>
            {loading && <Spinner />}
            <BrowserRouter>
              <MainAppBar></MainAppBar>
              <div className="container p-5">
                <Routes>
                  <Route exact path="/" element={<Home />} />
                  <Route
                    path="workers"
                    element={
                      <WorkersContextProvider>
                        <div className="md:flex gap-5">
                          <Workers />
                          <div className="flex-1">
                            <Outlet />
                          </div>
                        </div>
                      </WorkersContextProvider>
                    }
                  >
                    <Route path=":id" element={<Worker></Worker>} />
                  </Route>
                  <Route
                    path="items"
                    element={
                      <ItemsContextProvider>
                        <div className="md:flex gap-5">
                          <Items />
                          <div className="flex-1">
                            <Outlet />
                          </div>
                        </div>
                      </ItemsContextProvider>
                    }
                  >
                    <Route path=":id" element={<Item />} />
                  </Route>
                  <Route path="register" element={<Register />} />
                  <Route path="profile" element={<Profile />} />
                </Routes>
              </div>
            </BrowserRouter>
            <div className="p-5">v. {version}</div>
          </div>
        )}
      </SettingsConsumer>
    </>
  );
};

export default App;
