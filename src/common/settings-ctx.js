import axios from "axios";
import { createContext, useEffect, useState } from "react";
import pkg from "../../package.json";
import { useTranslation } from "react-i18next";

const SettingsContext = createContext();
const STORAGE_KEY = "token";
const themes = ["bg-gray-500", "bg-gray-100"];

const defaultSettings = {
  theme: themes[1],
  loggedIn: false,
  loading: false,
};

const setAuthToken = (token) => {
  localStorage.setItem(STORAGE_KEY, token);
  axios.defaults.headers.common["Authorization"] = token;
};

setAuthToken(localStorage.getItem(STORAGE_KEY));

const httpInterceptors = (loading, logged) => {
  axios.interceptors.request.use(
    (config) => {
      loading(true);
      return { ...config };
    },
    (error) => {
      loading(false);
      return Promise.reject(error);
    }
  );

  axios.interceptors.response.use(
    (response) => {
      loading(false);
      return response;
    },
    (error) => {
      alert(`SERVER ERROR\n${JSON.stringify(error.response.data, null, 4)}`);
      loading(false);
      logged(false);
      return Promise.reject(error);
    }
  );
};

export const SettingsProvider = ({ children, settings }) => {
  const [theme, setTheme] = useState(defaultSettings.theme);
  const [loggedIn, setLoggedIn] = useState(defaultSettings.loggedIn);
  const [loading, setLoading] = useState(defaultSettings.loading);
  const [lang, setLang] = useState("en");
  const toggleColor = (theme) => themes.filter((val) => theme !== val)[0];
  const toggleTheme = () => setTheme(toggleColor(theme));
  const { i18n } = useTranslation();

  useEffect(() => {
    httpInterceptors(setLoading, setLoggedIn);
  }, [setLoading]);

  useEffect(() => {
    i18n.changeLanguage(lang);
  }, [lang, i18n]);

  return (
    <SettingsContext.Provider
      value={{
        theme,
        toggleTheme,
        loggedIn,
        setLoggedIn,
        loading,
        setLoading,
        setAuthToken,
        version: pkg.version,
        lang,
        setLang,
      }}
    >
      {children}
    </SettingsContext.Provider>
  );
};

export const SettingsConsumer = SettingsContext.Consumer;

export default SettingsContext;
