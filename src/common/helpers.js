export function search(collection, filters) {

    if (!collection.length || !Object.keys(filters).length) return collection;

    return collection.filter((el) => {
        for (const key in filters) {
            const filter = filters[key]?.toString().toLowerCase();
            const value = el[key]?.toString().toLowerCase();
            if (!value?.includes(filter)) return false;
        }
        return true;
    })

}
