import * as React from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';

export default function DataGrid({ data, config, children, onAction }) {

    if (!data || !data.length) return 'no data';

    function action({ target: { innerText: type }, currentTarget: { dataset: { id: payload } } }) {
        onAction({ type, payload });
    }

    return (
      <TableContainer component={Paper}>
        <Table className="table-auto">
          <TableHead className="bg-gray-100">
            <TableRow className="uppercase text-bg-grey-dark">
              <TableCell className="p-4 text-xs text-gray-500">Nr</TableCell>
              {config.map((head, id) => (
                <TableCell key={id} className="p-4 text-xs text-gray-500">
                  {head.head || head.key}
                </TableCell>
              ))}
              {children && (
                <TableCell className="p-4 text-xs text-gray-500">
                  actions
                </TableCell>
              )}
            </TableRow>
          </TableHead>
          <TableBody className="bg-white">
            {data.map((item, idx) => (
              <TableRow key={item.id} className="m-1 whitespace-nowrap">
                <TableCell className="p-4">{idx + 1}</TableCell>
                {config.map((field, idx) => {
                  switch (field.type) {
                    case "input":
                      return (
                        <TableCell key={idx} className="p-4">
                          <input
                            type="text"
                            name={field.key}
                            data-id={item.id}
                            className={"form-control"}
                            defaultValue={item[field.key]}
                          />
                        </TableCell>
                      );
                    case "image":
                      return (
                        <TableCell key={idx} className="p-4">
                          <img src={item[field.key]} width="50" alt="img" />
                        </TableCell>
                      );
                    default:
                      return (
                        <TableCell key={idx} className="p-4">
                          {item[field.key]}
                        </TableCell>
                      );
                  }
                })}
                <TableCell onClick={action} data-id={item.id} className="p-4">
                  {children.map((el, idx) => (
                    <span key={idx} className="mx-1">
                      {el}
                    </span>
                  ))}
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    );

}
