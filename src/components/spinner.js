import { CircularProgress, Box } from "@mui/material";

export default function Spinner() {
  return (
    <>
      <Box
        sx={{
          position: "absolute",
          width: "100%",
          height: "100%",
          backgroundColor: "rgba(0,0,0,.6)",
          left: 0,
          zIndex: 100,
          top: 0,
          transition: "background-color 200ms linear",
          display: "flex",
        }}
      >
        <div className="m-auto">
          <CircularProgress color="warning" />
        </div>
      </Box>
    </>
  );
}
