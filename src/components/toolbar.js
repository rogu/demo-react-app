import {
  Button,
  Select,
  MenuItem,
  InputLabel,
  FormControl,
} from "@mui/material";
import { useContext } from "react";
import SettingsContext from "../common/settings-ctx";
import { Auth } from "./auth";
import { useTranslation } from "react-i18next";
import { styled } from "@mui/material/styles";

export const Tools = () => {
  const { toggleTheme, lang, setLang } = useContext(SettingsContext);
  const { t } = useTranslation();

  const handleToggleTheme = () => {
    toggleTheme();
  };

  const handleToggleLang = (ev) => {
    setLang(ev.target.value);
  };

  const StyledSelect = styled(Select)`
    .MuiFormControl-root {
      label {
        /* margin-top: 3px; */
      }
    }
    width: 100px;
    //margin-top: 5px;
    //top: 5px;
    & > div {
      padding: 7px;
    }
  `;

  return (


      <div className="md:flex gap-5 items-center justify-items-end">
        <Button
          variant="contained"
          color="info"
          size="small"
          onClick={handleToggleTheme}
        >
          change theme
        </Button>
        <Auth></Auth>

        <h1>{t("Welcome to React")}</h1>
        <FormControl>
          <InputLabel>Lang</InputLabel>
          <StyledSelect
            label="Lang"
            value={lang}
            onChange={handleToggleLang}
          >
            {["pl", "en"].map((v) => (
              <MenuItem key={v} value={v}>
                {v}
              </MenuItem>
            ))}
          </StyledSelect>
        </FormControl>
      </div>

  );
};
