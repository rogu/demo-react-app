import { MenuItem } from "@mui/material";
import { NavLink } from "react-router-dom";

export default function Nav() {
  return (
    <>
      <MenuItem>
        <NavLink className="nav-btn" to="/">
          home
        </NavLink>
      </MenuItem>

      <MenuItem>
        <NavLink className="nav-btn" to="/items">
          items
        </NavLink>
      </MenuItem>

      <MenuItem>
        <NavLink className="nav-btn" to="/workers">
          workers
        </NavLink>
      </MenuItem>

      <MenuItem>
        <NavLink className="nav-btn" to="/register">
          register
        </NavLink>
      </MenuItem>
    </>
  );
}
