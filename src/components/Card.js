import * as React from "react";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Typography from "@mui/material/Typography";

export default function MediaCard({ title, img, children }) {
  return (
    <Card sx={{ }}>
      {img && <CardMedia component="img" height="140" image={img} />}
      <CardContent>
        <Typography gutterBottom variant="h5" component="div">
          {title}
        </Typography>
        {children}
      </CardContent>
      {/* <CardActions>
        <Button size="small" onClick={action}>
          {btn}
        </Button>
      </CardActions> */}
    </Card>
  );
}
