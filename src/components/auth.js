import { Button, TextField } from "@mui/material";
import { useContext, useEffect } from "react";
import axios from "axios";
import SettingsContext from "../common/settings-ctx";
import { Api } from "../common/api";
import { Controller, useForm } from "react-hook-form";

export const Auth = () => {
  const { loggedIn, setLoggedIn, setAuthToken } = useContext(SettingsContext);
  const { handleSubmit, control } = useForm();

  function logIn(data) {
    axios.post(Api.LOGIN_END_POINT, data).then((resp) => {
      setLoggedIn(true);
      setAuthToken(resp.data.accessToken);
    });
  }

  useEffect(() => {
    axios
      .get(Api.LOGGED_END_POINT)
      .then(({ data: { error } }) => setLoggedIn(error ? false : true));
  }, [setLoggedIn]);

  function logOut() {
    setLoggedIn(false);
    setAuthToken("");
  }

  return (
    <div>
      {loggedIn ? (
        <Button variant="outlined" color="secondary" onClick={logOut}>
          log out
        </Button>
      ) : (
        <form onSubmit={handleSubmit(logIn)} className="flex gap-2 w-72">
          <Controller
            name="username"
            control={control}
            defaultValue="admin@localhost"
            rules={{ required: "Username required" }}
            render={({ field: { onChange, value } }) => (
              <TextField
                label="Username"
                variant="outlined"
                value={value}
                size="small"
                onChange={onChange}
              />
            )}
          />
          <Controller
            name="password"
            control={control}
            defaultValue="Admin1"
            rules={{ required: "Password required" }}
            render={({ field: { onChange, value } }) => (
              <TextField
                label="Password"
                variant="outlined"
                value={value}
                size="small"
                onChange={onChange}
              />
            )}
          />
          <Button
            type="submit"
            size="small"
            variant="contained"
            color="primary"
          >
            log&nbsp;in
          </Button>
        </form>
      )}
    </div>
  );
};
