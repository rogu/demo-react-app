import * as React from "react";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import IconButton from "@mui/material/IconButton";
import Menu from "@mui/material/Menu";
import MenuIcon from "@mui/icons-material/Menu";
import MoreIcon from "@mui/icons-material/MoreVert";
import Nav from "./nav";
import { Tools } from "./toolbar";
import { NavLink } from "react-router-dom";
import { MenuItem } from "@mui/material";
import { useState } from "react";

export default function MainAppBar() {
  const [anchorLeft, setAnchorLeft] = useState(null);
  const [anchorRight, setAnchorRight] = useState(null);

  const isLeftMenuOpen = Boolean(anchorLeft);
  const isRightMenuOpen = Boolean(anchorRight);

  const handleRightMenuOpen = (event) => {
    setAnchorRight(event.currentTarget);
  };

  const handleLeftMenuOpen = (event) => {
    setAnchorLeft(event.currentTarget);
  };

  const handleAllMenuClose = () => {
    setAnchorRight(null);
    setAnchorLeft(null);
  };

  const renderRightMenu = (
    <Menu
      anchorEl={anchorRight}
      anchorOrigin={{
        vertical: "top",
        horizontal: "right",
      }}
      keepMounted
      transformOrigin={{
        vertical: "top",
        horizontal: "right",
      }}
      open={isRightMenuOpen}
      onClose={handleAllMenuClose}
    >
      <MenuItem onClick={handleAllMenuClose}>
        <NavLink to="profile">profile</NavLink>
      </MenuItem>
      <Box sx={{ padding: 2 }}>
        <Tools></Tools>
      </Box>
    </Menu>
  );

  const renderLeftMenu = (
    <Menu
      anchorEl={anchorLeft}
      anchorOrigin={{
        vertical: "top",
        horizontal: "left",
      }}
      transformOrigin={{
        vertical: "top",
        horizontal: "left",
      }}
      open={isLeftMenuOpen}
      onClose={handleAllMenuClose}
      onClick={handleAllMenuClose}
    >
      <Nav></Nav>
    </Menu>
  );

  return (
    <Box sx={{}}>
      <AppBar position="static">
        <Toolbar>
          <Box sx={{ display: { xs: "flex", md: "none" } }}>
            <IconButton
              size="large"
              edge="start"
              color="inherit"
              onClick={handleLeftMenuOpen}
              aria-label="open drawer"
              sx={{ mr: 2 }}
            >
              <MenuIcon />
            </IconButton>
          </Box>

          <Box sx={{ display: { xs: "none", md: "flex" } }}>
            <Nav></Nav>
          </Box>

          <Box sx={{ flexGrow: 1 }} />
          <Box sx={{ display: { xs: "none", md: "flex" } }}>
            <Tools></Tools>
          </Box>
          <Box sx={{ display: { xs: "flex", md: "none" } }}>
            <IconButton
              size="large"
              aria-label="show more"
              aria-haspopup="true"
              onClick={handleRightMenuOpen}
              color="inherit"
            >
              <MoreIcon />
            </IconButton>
          </Box>
        </Toolbar>
      </AppBar>
      {renderRightMenu}
      {renderLeftMenu}
    </Box>
  );
}
