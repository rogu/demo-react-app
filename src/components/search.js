import {
  Button,
  TextField,
  Slider,
  Select,
  MenuItem,
  Box,
  styled,
  FormControl,
  InputLabel,
} from "@mui/material";
import { useCallback, useEffect, useRef, useState } from "react";
import MediaCard from "./Card";

export default function Search({ controls, update }) {
  const [state, setState] = useState({});
  const mounted = useRef(false);

  const setDefault = useCallback((controls) => {
    setState(() => {
      const next = controls.reduce(
        (acc, { key, currentValue }) => ({
          ...acc,
          [key]: currentValue || "",
        }),
        {}
      );
      mounted.current ? update(next) : (mounted.current = true);
      return next;
    });
  }, [update]);

  useEffect(() => {
    setDefault(controls);
  }, [setDefault, controls]);

  const clear = useCallback(
    (ev) => {
      ev.preventDefault();
      setDefault(controls);
    },
    [setDefault, controls]
  );

  const up = useCallback(
    ({ target: { name, value } }) => {
      setState((prev) => {
        const next = { ...prev, [name]: value };
        update(next);
        return next;
      });
    },
    [setState, update]
  );

  const data = {
    title: "Search items",
    btn: "clear",
    action: clear,
  };

  const StyledSelect = styled(Select)`
    width: 100px;
    & > div {
      /* padding: 7px; */
    }
  `;

  const getComponent = ({ key, type, value }) => {
    switch (type) {
      case "slider":
        return (
          <div key={key} className="mx-2">
            {key}
            <Slider
              defaultValue={50}
              aria-label="Default"
              valueLabelDisplay="auto"
              value={+state[key]}
              label={key}
              name={key}
              onChange={up}
            />
          </div>
        );
      case "select":
        return (
          <FormControl key={key}>
            <InputLabel>{key}</InputLabel>
            <StyledSelect
              value={state[key]}
              size="small"
              label="Items Per Page"
              name={key}
              onChange={up}
            >
              {value.map((option) => (
                <MenuItem key={option} value={option}>
                  {option}
                </MenuItem>
              ))}
            </StyledSelect>
          </FormControl>
        );
      default:
        return (
          <TextField
            label={key}
            size="small"
            key={key}
            name={key}
            value={state[key]}
            onChange={up}
          ></TextField>
        );
    }
  };

  return (
    <MediaCard {...data}>
      {/* {JSON.stringify(state)} */}
      <form onSubmit={clear}>
        {Object.keys(state).length &&
          controls.map((obj, i) => (
            <Box key={i} sx={{ marginBottom: 3 }}>
              {getComponent(obj)}
            </Box>
          ))}
        <Button variant="contained" type="submit">
          clear
        </Button>
      </form>
    </MediaCard>
  );
}
