import axios from "axios";
import {
  createContext,
  useCallback,
  useEffect,
  useReducer,
  useState,
} from "react";
import { Api } from "../../common/api";
import workersReducer, { Actions } from "./workers-reducer";

export const WorkersContext = createContext();

export const WorkersContextProvider = ({ children, settings }) => {
  const [workers, dispatch] = useReducer(workersReducer, []);
  const [filters, setFilters] = useState({});

  useEffect(() => {
    dispatch({ type: Actions.SEARCH_WORKERS, payload: filters });
  }, [filters]);

  const fetchWorkers = useCallback(async () => {
    const {
      data: { data },
    } = await axios.get(Api.WORKERS_END_POINT);
    dispatch({
      type: Actions.SAVE_WORKERS,
      payload: data,
    });
  }, [dispatch]);

  const filterWorkers = useCallback(
    (filters) => {
      setFilters((prev) => ({ ...prev, ...filters }));
    },
    [setFilters]
  );

  const removeWorker = useCallback(
    async (worker) => {
      await axios.delete(`${Api.WORKERS_END_POINT}/${worker.id}`);
      dispatch({ type: Actions.REMOVE_WORKER, payload: worker });
    },
    [dispatch]
  );

  const addWorker = useCallback(
    async (worker) => {
      await axios.post(Api.WORKERS_END_POINT, worker);
      fetchWorkers();
    },
    [fetchWorkers]
  );

  useEffect(() => fetchWorkers(), [fetchWorkers]);

  return (
    <WorkersContext.Provider
      value={{
        workers,
        filterWorkers,
        removeWorker,
        addWorker,
      }}
    >
      {children}
    </WorkersContext.Provider>
  );
};
export const WorkersConsumer = WorkersContext.Consumer;
