import { search } from "../../common/helpers";

export const Actions = {
  SAVE_WORKERS: "SAVE_WORKERS",
  SEARCH_WORKERS: "SEARCH_WORKERS",
  ADD_WORKER: "ADD_WORKERS",
  REMOVE_WORKER: "REMOVE_WORKERS",
};

let backup = [];

const makeBackup = (state) => {
  return (backup = state);
};

export default function reducer(state, action) {
  switch (action.type) {
    case Actions.SAVE_WORKERS:
      return makeBackup(action.payload);
    case Actions.SEARCH_WORKERS:
      return search(backup, action.payload);
    case Actions.ADD_WORKER:
      const addedWorkers = [...state, action.payload];
      return makeBackup(addedWorkers);
    case Actions.REMOVE_WORKER:
      const filteredWorkers = state.filter((t) => t.id !== action.payload.id);
      return makeBackup(filteredWorkers);
    default:
      return state;
  }
}
