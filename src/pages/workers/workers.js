import React, { useMemo, useContext, useRef } from "react";
import Search from "../../components/search";
import Grid from "../../components/grid";
import { useNavigate } from "react-router-dom";
import { WorkersContext } from "./workers-ctx";
import SimpleModal from "../../components/modal";
import Button from "@mui/material/Button";
import MediaCard from "../../components/Card";

export const Workers = () => {
  const { workers, filterWorkers, removeWorker, addWorker } =
    useContext(WorkersContext);
  const navigate = useNavigate();
  const addModal = useRef(null);
  const config = useMemo(
    () => [{ key: "name" }, { key: "phone", type: "input" }],
    []
  );
  const filtersConfig = useMemo(() => [{ key: "name" }, { key: "phone" }], []);

  async function onAction({ type, payload }) {
    switch (type.toLowerCase()) {
      case "more":
        navigate(`${payload}`);
        break;
      case "remove":
        removeWorker({ id: payload });
        break;
      case "add":
        addWorker(payload);
        break;
      default:
        break;
    }
  }

  function send(ev) {
    ev.preventDefault();
    const { name, phone, category } = ev.target;
    const [type, payload] = [
      "add",
      { name: name.value, phone: phone.value, category: category.value },
    ];
    onAction({ type, payload });
  }

  return (
    <>
      <div className="md:flex gap-5">
        <Search controls={filtersConfig} update={filterWorkers}></Search>
        <div>
          <MediaCard title="Workers list">
            <SimpleModal ref={addModal} text="add worker">
              <form onSubmit={send}>
                <input type="text" name="name" />
                <input type="number" name="phone" />
                <select name="category">
                  <option value="support">support</option>
                  <option value="sales">sales</option>
                </select>
                <br />
                <Button type="submit" variant="contained">
                  send
                </Button>
              </form>
            </SimpleModal>
            <div className="mb-5"></div>
            <Grid data={workers} config={config} onAction={onAction}>
              <Button variant="contained" color="secondary">
                more
              </Button>
              <Button variant="contained">remove</Button>
            </Grid>
          </MediaCard>
        </div>
      </div>
    </>
  );
};
