import { Button } from "@mui/material";
import { useContext } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { WorkersContext } from "./workers-ctx";

export function Worker() {
  const { workers } = useContext(WorkersContext);
  const params = useParams();
  const worker = workers.find(({ id }) => id === params.id);
  const navigate = useNavigate();

  return (
    <>
      {worker ? (
        <div className="max-w-md py-4 px-8 bg-white shadow-lg rounded-lg my-20">
          <div className="flex justify-center md:justify-end -mt-16">
            <img
              className="w-20 h-20 object-cover rounded-full border-2 border-indigo-500"
              alt="img"
              src="https://api.debugger.pl/assets/admin.png"
            />
          </div>
          <div>
            <h2 className="text-gray-800 text-3xl font-semibold">
              {worker.name}
            </h2>
            <p className="mt-2 text-gray-600">tel. {worker.phone}</p>
          </div>
          <div className="flex justify-end mt-4">{worker.category}</div>
          <Button variant="contained" color="primary" onClick={navigate.goBack}>
            back
          </Button>
        </div>
      ) : (
        "no data"
      )}
    </>
  );
}
