import { Button, TextField, FormControl } from "@mui/material";
import { useForm } from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import styled from "styled-components";
import { useTranslation } from "react-i18next";

const Field = styled(FormControl)({
  color: "darkslategray",
  background: "linear-gradient(45deg,lightgray 5%, #fafafa 90%)",
  padding: 20,
  borderRadius: 4,
  border: "1px solid lightgray",
  display: "block",
  marginBottom: 15,
});

export function Register() {
  const { t } = useTranslation();
  const schema = yup.object().shape({
    username: yup.string().required().email(),
    password: yup
      .string()
      .required()
      .matches(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{4,8}$/),
    passwordConfirmation: yup
      .string()
      .oneOf([yup.ref("password")], "passwords aren't equals"),
    hobbies: yup
      .array()
      .test("test1", null, (val) => {
        return val?.length
          ? true
          : new yup.ValidationError(
              "Please check at least one checkbox",
              "err",
              "hobbies"
            );
      })
      .required()
      .nullable(),
    birthDate: yup
      .string()
      .test("testDate", "date should be passed", (val) => {
        return Date.parse(val) < Date.now();
      })
      .required(),
  });

  const {
    handleSubmit,
    formState: { errors },
    register,
  } = useForm({ resolver: yupResolver(schema) });

  const onSubmit = (val) => {
    console.log(val);
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <Field>
        <div>{t("username")}</div>
        <TextField
          {...register("username", { required: "asdf" })}
          variant="standard"
        />
        <div className="text-red-500">{errors?.username?.message}</div>
      </Field>

      <Field>
        <div>{t("password")}</div>
        <input
          className={`${errors.password ? "border border-red-400" : ""}`}
          type="text"
          {...register("password")}
        />
        <div className="text-red-500">{errors?.password?.message}</div>
      </Field>

      <Field>
        <div>{t("passwordConfirm")}</div>
        <input
          type="text"
          {...register("passwordConfirmation")}
        />
        <div className="text-red-500">
          {errors?.passwordConfirmation?.message}
        </div>
      </Field>

      <Field>
        <div>{t("hobbies")}</div>

        <label className="mr-4 inline">
          music
          <input
            type="checkbox"
            value="music"
            {...register("hobbies")}
          />
        </label>
        <label className="mr-4 inline">
          sport
          <input
            type="checkbox"
            value="sport"
            {...register("hobbies")}
          />
        </label>

        <div className="text-red-500">{errors?.hobbies?.message}</div>
      </Field>

      <Field>
        <label>
          birth date
          <input type="date" {...register("birthDate")} />
        </label>
        <div className="text-red-500">{errors?.birthDate?.message}</div>
      </Field>

      <Button type="submit" variant="contained" color="info">
        send
      </Button>
    </form>
  );
}
