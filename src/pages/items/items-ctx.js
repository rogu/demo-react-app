import axios from "axios";
import { createContext, useCallback, useEffect, useState } from "react";
import { Api } from "../../common/api";
import qs from "qs";
import { useDebounce } from "@react-hook/debounce";

export const ItemsContext = createContext();

export function ItemsContextProvider({ children }) {
  const [state, setState] = useState({
    data: [],
    total: 0,
  });
  const [filters, setFilters] = useDebounce(
    {
      itemsPerPage: 5,
      currentPage: 1,
    },
    500
  );

  const fetchItems = useCallback(
    async (filters) => {
      const { data } = await axios.get(
        `${Api.ITEMS_END_POINT}?${qs.stringify(filters)}`
      );
      setState(data);
      if (
        data.total &&
        Math.ceil(data.total / filters.itemsPerPage) < filters.currentPage
      )
        setFilters((prevState) => ({ ...prevState, currentPage: 1 }));
    },
    [setFilters]
  );

  const removeItem = useCallback(
    async (payload) => {
      await axios.delete(`${Api.ITEMS_END_POINT}/${payload}`);
      fetchItems(filters);
    },
    [fetchItems, filters]
  );

  const addItem = useCallback(
    async (item) => {
      const res = await axios.post(Api.ITEMS_END_POINT, item);
      fetchItems(filters);
      return res;
    },
    [fetchItems, filters]
  );

  useEffect(() => fetchItems(filters), [filters, fetchItems]);

  return (
    <ItemsContext.Provider
      value={{ state, setState, removeItem, addItem, filters, setFilters }}
    >
      {children}
    </ItemsContext.Provider>
  );
}

export const ItemsContextConsumer = ItemsContext.Consumer;
