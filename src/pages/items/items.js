import { useContext, useMemo, useRef } from "react";
import DataGrid from "../../components/grid";
import { useNavigate } from "react-router-dom";
import Pagination from "@mui/material/Pagination";
import SimpleModal from "../../components/modal";
import { Button, FormControl, InputLabel, Chip } from "@mui/material";
import SettingsContext from "../../common/settings-ctx";
import AddItemForm from "./add-item-form";
import Search from "../../components/search";
import { ItemsContext } from "./items-ctx";
import MediaCard from "../../components/Card";
import { useCallback } from "react";

export const Items = () => {
  const { state, removeItem, addItem, filters, setFilters } =
    useContext(ItemsContext);
  const { loggedIn } = useContext(SettingsContext);
  const navigate = useNavigate();
  const addItemModal = useRef(null);

  const config = useMemo(
    () => [
      { key: "title" },
      { key: "price", type: "input" },
      { key: "imgSrc", type: "image", head: "image" },
    ],
    []
  );

  const updateFilters = useCallback(
    (filters) => {
      setFilters((prevState) => ({ ...prevState, ...filters }));
    },
    [setFilters]
  );

  const setPage = (ev, currentPage) => {
    updateFilters({ currentPage });
  };

  const onAction = async ({ type, payload }) => {
    switch (type.toLowerCase()) {
      case "more":
        navigate(`${payload}`);
        break;
      case "remove":
        removeItem(payload);
        break;
      case "add":
        const res = await addItem(payload);
        if (res.status === 201) addItemModal.current?.handleClose();
        break;
      default:
        break;
    }
  };

  const filtersConfig = useMemo(
    () => [
      { key: "title" },
      { key: "priceFrom", type: "slider" },
      {
        key: "itemsPerPage",
        type: "select",
        value: [2, 5, 10],
        currentValue: 5,
      },
    ],
    []
  );

  return (
    <>
      <div className="md:flex gap-5">
        <Search controls={filtersConfig} update={updateFilters}></Search>
        <div>
          <MediaCard title="Items list">
            {/* {JSON.stringify(filters)} */}
            <div className="md:flex gap-5 mb-5">
              <SimpleModal ref={addItemModal} text="add item">
                <AddItemForm addItem={onAction}></AddItemForm>
              </SimpleModal>
              <FormControl variant="outlined">
                <InputLabel>Items Per Page</InputLabel>
              </FormControl>

              <Chip label={"total: " + state.total} />

              <Pagination
                page={filters.currentPage}
                onChange={setPage}
                count={Math.ceil(state.total / +filters.itemsPerPage)}
                color="primary"
              />
            </div>

            <DataGrid
              data={state.data}
              config={config}
              loggedIn={loggedIn}
              onAction={onAction}
            >
              {loggedIn && (
                <Button variant="contained" color="secondary">
                  remove
                </Button>
              )}
              <Button variant="contained" color="secondary">
                more
              </Button>
            </DataGrid>
          </MediaCard>
        </div>
      </div>
    </>
  );
};
