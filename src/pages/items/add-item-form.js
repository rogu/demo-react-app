import {
  Button,
  FormControl,
  FormLabel,
  MenuItem,
  Select,
  TextField,
} from "@mui/material";
import { useState } from "react";

export default function AddItemForm({ addItem }) {
  const [form, setForm] = useState({ title: "", price: "", category: "" });

  function updateForm({ target: { name, value } }) {
    setForm((prev) => ({ ...prev, [name]: value }));
  }

  function send(ev) {
    ev.preventDefault();
    const [type, payload] = [
      "add",
      { title: form.title, price: form.price, category: form.category },
    ];
    addItem({ type, payload });
  }

  const empty = "Empty field!";

  return (
    <>
      <div className="my-5">
        <FormControl>
          <TextField
            autoFocus
            value={form.title}
            onChange={updateForm}
            name="title"
            size="small"
            label="title"
            error={form.title === ""}
            helperText={form.title === "" ? empty : " "}
          />
        </FormControl>
      </div>
      <div className="my-5">
        <FormControl>
          <TextField
            value={form.price}
            onChange={updateForm}
            name="price"
            label="price"
            size="small"
            error={form.price === ""}
            helperText={form.price === "" ? empty : " "}
          />
        </FormControl>
      </div>
      <div className="my-5">
        <FormControl style={{ minWidth: 120 }}>
          <FormLabel>category</FormLabel>
          <Select
            value={form.category}
            name="category"
            size="small"
            onChange={updateForm}
          >
            {["food", "clothes"].map((option) => (
              <MenuItem key={option} value={option}>
                {option}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
      </div>

      <Button variant="outlined" type="submit" onClick={send}>
        send
      </Button>
    </>
  );
}
