import { useContext, useMemo } from "react";
import { ItemsContext } from "./items-ctx";
import { useParams } from "react-router-dom";
import MediaCard from "../../components/Card";

export default function Item() {
  const { state } = useContext(ItemsContext);
  const params = useParams();
  const item = state.data.find((i) => i.id === params.id);
  const data = useMemo(() => ({ ...item, img: item?.imgSrc }), [item]);
  const render = useMemo(
    () => (
      <div className="flex gap-5">
        <div className="font-bold">
          <div>title: {data?.title}</div>
          <div>price: {data?.price}</div>
        </div>
      </div>
    ),
    [data]
  );
  return <MediaCard {...data}>{render}</MediaCard>;
}
